module gitlab.com/qomarullah/mock-pa

go 1.15

require (
	github.com/labstack/echo/v4 v4.4.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
