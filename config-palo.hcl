log_level = "info"

driver "terraform" {
  log = true
  required_providers {
    panos = {
      source = "PaloAltoNetworks/panos"
    }
  }
}

consul {
  address = "localhost:8500"
}

terraform_provider "panos" {
  alias = "panos1" 
  hostname = "localhost:3000" 
  username = "admin" 
  api_key  = "qazwsx" 
}

task {
  name = "Create_DAG_on_PANOS1"
  description = "Dynamic Address Groups based on service definition"
  source = "github.com/PaloAltoNetworks/terraform-panos-dag-nia"
  providers = ["panos.panos1"]
  services = ["web", "webapi"]
  variable_files = []
}