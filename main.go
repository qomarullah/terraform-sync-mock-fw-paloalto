package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func api(c echo.Context) error {
	var bodyBytes []byte
	if c.Request().Body != nil {
		bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
	}
	response := ""
	fmt.Println("command:" + string(bodyBytes))
	cmd := string(bodyBytes)

	if strings.Contains(cmd, "register") {
		response = `
				<response status="success">
				<result>
				<entry ip="1.1.1.1" from_agent="0" persistent="1">
					<tag>
					<member>service-name</member>
					</tag>
				</entry>
				<count>1</count>
				</result>
			</response>
		`
	} else {
		response = `
	<response status="success">
	<result>
		<system>
			<hostname>PA-3050-A</hostname>
			<ip-address>10.2.3.4</ip-address>
			<public-ip-address>unknown</public-ip-address>
			<netmask>255.255.252.0</netmask>
			<default-gateway>10.2.3.1</default-gateway>
			<is-dhcp>no</is-dhcp>
			<ipv6-address>unknown</ipv6-address>
			<ipv6-link-local-address>c123::21b:ffff:feff:c1234/64</ipv6-link-local-address>
			<ipv6-default-gateway></ipv6-default-gateway>
			<mac-address>00:00:00:ff:c7:00</mac-address>
			<time>Tue Jan  8 16:22:56 2019</time>
			<uptime>0 days, 18:28:38</uptime>
			<devicename>PA-3050-A</devicename>
			<family>3000</family>
			<model>PA-3050</model>
			<serial>001701000529</serial>
			<cloud-mode>non-cloud</cloud-mode>
			<sw-version>9.0.0-b36</sw-version>
			<global-protect-client-package-version>0.0.0</global-protect-client-package-version>
			<app-version>8111-5239</app-version>
			<app-release-date>2019/01/07 15:51:30 PST</app-release-date>
			<av-version>3328-3783</av-version>
			<av-release-date>2019/01/07 11:22:02 PST</av-release-date>
			<threat-version>8111-5239</threat-version>
			<threat-release-date>2019/01/07 15:51:30 PST</threat-release-date>
			<wf-private-version>0</wf-private-version>
			<wf-private-release-date>unknown</wf-private-release-date>
			<url-db>paloaltonetworks</url-db>
			<wildfire-version>0</wildfire-version>
			<wildfire-release-date></wildfire-release-date>
			<url-filtering-version>20190109.20005</url-filtering-version>
			<global-protect-datafile-version>unknown</global-protect-datafile-version>
			<global-protect-datafile-release-date>unknown</global-protect-datafile-release-date>
			<global-protect-clientless-vpn-version>0</global-protect-clientless-vpn-version>
			<global-protect-clientless-vpn-release-date></global-protect-clientless-vpn-release-date>
			<logdb-version>9.0.10</logdb-version>
			<platform-family>3000</platform-family>
			<vpn-disable-mode>off</vpn-disable-mode>
			<multi-vsys>on</multi-vsys>
			<operational-mode>normal</operational-mode>
		</system>
	</result>
</response>
		`
	}
	fmt.Println("response", response)
	return c.HTML(http.StatusOK, response)
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}
	e := echo.New()
	e.Use(middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {
		e.Logger.Info("request", reqBody)
		e.Logger.Info("response", resBody)

	}))

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.POST("/api", api)
	e.GET("/api", api)

	// Start server
	//e.Logger.Fatal(e.Start(":3000"))
	if err := e.StartTLS(":"+port, "server.crt", "server.key"); err != http.ErrServerClosed {
		log.Fatal(err)
	}

}

//ssl
//openssl genrsa -out server.key 2048
//openssl ecparam -genkey -name secp384r1 -out server.key
//openssl req -new -x509 -sha256 -key server.key -out server.crt -days 3650
